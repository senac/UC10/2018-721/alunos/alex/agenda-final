package br.com.senac.view;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public abstract class JFrameMensagens extends JFrame {

    public JFrameMensagens() {
        this.setVisible(true);
    }

    public void showMessageInformacao(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "Aviso", JOptionPane.INFORMATION_MESSAGE);
    }

    public void showMessageErro(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
    }

    public void showMessageAlerta(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "Alerta", JOptionPane.WARNING_MESSAGE);
    }

}

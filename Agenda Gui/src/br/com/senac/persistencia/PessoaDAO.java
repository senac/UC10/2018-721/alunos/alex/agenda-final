package br.com.senac.persistencia;

import br.com.senac.model.Pessoa;
import br.com.senac.schedule.persistence.DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PessoaDAO implements DAO<Pessoa> {

    @Override
    public void inserir(Pessoa pessoa) {
        Connection connection = null;

        try {
            connection = Conexao.getConnection();
            String query = "INSERT INTO CONTATOS (NOME , TELEFONE ) VALUES (? , ?)";
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, pessoa.getNome());
            ps.setString(2, pessoa.getNumero());

            int linha = ps.executeUpdate();
            if (linha > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                int id = rs.getInt(1);
                pessoa.setId(id);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void atualizar(Pessoa pessoa) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Pessoa> listaTodos() {

        Connection connection = null;
        List<Pessoa> lista = new ArrayList<>();
        try {

            connection = Conexao.getConnection();
            String query = "SELECT * FROM CONTATOS ";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                String telefone = rs.getString("telefone");
                Pessoa pessoa = new Pessoa(id, nome, telefone);
                lista.add(pessoa);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return lista;
    }

    @Override
    public Pessoa buscarPorId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Pessoa> buscarPorNome(String nome) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getQuantidadeContatos() {
        Connection connection = null;
        int quantidade = 0;
        try {

            connection = Conexao.getConnection();
            String query = "SELECT count(1) as quantidade FROM CONTATOS ";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            rs.first();
            quantidade = rs.getInt("quantidade");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return quantidade;
    }

}
